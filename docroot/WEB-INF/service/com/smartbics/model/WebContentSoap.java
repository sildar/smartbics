/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartbics.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author ILDAR
 * @generated
 */
public class WebContentSoap implements Serializable {
	public static WebContentSoap toSoapModel(WebContent model) {
		WebContentSoap soapModel = new WebContentSoap();

		soapModel.setWebContentId(model.getWebContentId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setUserGroupId(model.getUserGroupId());
		soapModel.setUserGroupName(model.getUserGroupName());
		soapModel.setFolderId(model.getFolderId());
		soapModel.setFolderName(model.getFolderName());

		return soapModel;
	}

	public static WebContentSoap[] toSoapModels(WebContent[] models) {
		WebContentSoap[] soapModels = new WebContentSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static WebContentSoap[][] toSoapModels(WebContent[][] models) {
		WebContentSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new WebContentSoap[models.length][models[0].length];
		}
		else {
			soapModels = new WebContentSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static WebContentSoap[] toSoapModels(List<WebContent> models) {
		List<WebContentSoap> soapModels = new ArrayList<WebContentSoap>(models.size());

		for (WebContent model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new WebContentSoap[soapModels.size()]);
	}

	public WebContentSoap() {
	}

	public long getPrimaryKey() {
		return _webContentId;
	}

	public void setPrimaryKey(long pk) {
		setWebContentId(pk);
	}

	public long getWebContentId() {
		return _webContentId;
	}

	public void setWebContentId(long webContentId) {
		_webContentId = webContentId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getUserGroupId() {
		return _userGroupId;
	}

	public void setUserGroupId(long userGroupId) {
		_userGroupId = userGroupId;
	}

	public String getUserGroupName() {
		return _userGroupName;
	}

	public void setUserGroupName(String userGroupName) {
		_userGroupName = userGroupName;
	}

	public long getFolderId() {
		return _folderId;
	}

	public void setFolderId(long folderId) {
		_folderId = folderId;
	}

	public String getFolderName() {
		return _folderName;
	}

	public void setFolderName(String folderName) {
		_folderName = folderName;
	}

	private long _webContentId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _userGroupId;
	private String _userGroupName;
	private long _folderId;
	private String _folderName;
}
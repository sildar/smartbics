/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartbics.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link WebContent}.
 * </p>
 *
 * @author ILDAR
 * @see WebContent
 * @generated
 */
public class WebContentWrapper implements WebContent, ModelWrapper<WebContent> {
	public WebContentWrapper(WebContent webContent) {
		_webContent = webContent;
	}

	@Override
	public Class<?> getModelClass() {
		return WebContent.class;
	}

	@Override
	public String getModelClassName() {
		return WebContent.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("webContentId", getWebContentId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("userGroupId", getUserGroupId());
		attributes.put("userGroupName", getUserGroupName());
		attributes.put("folderId", getFolderId());
		attributes.put("folderName", getFolderName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long webContentId = (Long)attributes.get("webContentId");

		if (webContentId != null) {
			setWebContentId(webContentId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long userGroupId = (Long)attributes.get("userGroupId");

		if (userGroupId != null) {
			setUserGroupId(userGroupId);
		}

		String userGroupName = (String)attributes.get("userGroupName");

		if (userGroupName != null) {
			setUserGroupName(userGroupName);
		}

		Long folderId = (Long)attributes.get("folderId");

		if (folderId != null) {
			setFolderId(folderId);
		}

		String folderName = (String)attributes.get("folderName");

		if (folderName != null) {
			setFolderName(folderName);
		}
	}

	/**
	* Returns the primary key of this web content.
	*
	* @return the primary key of this web content
	*/
	@Override
	public long getPrimaryKey() {
		return _webContent.getPrimaryKey();
	}

	/**
	* Sets the primary key of this web content.
	*
	* @param primaryKey the primary key of this web content
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_webContent.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the web content ID of this web content.
	*
	* @return the web content ID of this web content
	*/
	@Override
	public long getWebContentId() {
		return _webContent.getWebContentId();
	}

	/**
	* Sets the web content ID of this web content.
	*
	* @param webContentId the web content ID of this web content
	*/
	@Override
	public void setWebContentId(long webContentId) {
		_webContent.setWebContentId(webContentId);
	}

	/**
	* Returns the group ID of this web content.
	*
	* @return the group ID of this web content
	*/
	@Override
	public long getGroupId() {
		return _webContent.getGroupId();
	}

	/**
	* Sets the group ID of this web content.
	*
	* @param groupId the group ID of this web content
	*/
	@Override
	public void setGroupId(long groupId) {
		_webContent.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this web content.
	*
	* @return the company ID of this web content
	*/
	@Override
	public long getCompanyId() {
		return _webContent.getCompanyId();
	}

	/**
	* Sets the company ID of this web content.
	*
	* @param companyId the company ID of this web content
	*/
	@Override
	public void setCompanyId(long companyId) {
		_webContent.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this web content.
	*
	* @return the user ID of this web content
	*/
	@Override
	public long getUserId() {
		return _webContent.getUserId();
	}

	/**
	* Sets the user ID of this web content.
	*
	* @param userId the user ID of this web content
	*/
	@Override
	public void setUserId(long userId) {
		_webContent.setUserId(userId);
	}

	/**
	* Returns the user uuid of this web content.
	*
	* @return the user uuid of this web content
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContent.getUserUuid();
	}

	/**
	* Sets the user uuid of this web content.
	*
	* @param userUuid the user uuid of this web content
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_webContent.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this web content.
	*
	* @return the user name of this web content
	*/
	@Override
	public java.lang.String getUserName() {
		return _webContent.getUserName();
	}

	/**
	* Sets the user name of this web content.
	*
	* @param userName the user name of this web content
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_webContent.setUserName(userName);
	}

	/**
	* Returns the create date of this web content.
	*
	* @return the create date of this web content
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _webContent.getCreateDate();
	}

	/**
	* Sets the create date of this web content.
	*
	* @param createDate the create date of this web content
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_webContent.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this web content.
	*
	* @return the modified date of this web content
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _webContent.getModifiedDate();
	}

	/**
	* Sets the modified date of this web content.
	*
	* @param modifiedDate the modified date of this web content
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_webContent.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the user group ID of this web content.
	*
	* @return the user group ID of this web content
	*/
	@Override
	public long getUserGroupId() {
		return _webContent.getUserGroupId();
	}

	/**
	* Sets the user group ID of this web content.
	*
	* @param userGroupId the user group ID of this web content
	*/
	@Override
	public void setUserGroupId(long userGroupId) {
		_webContent.setUserGroupId(userGroupId);
	}

	/**
	* Returns the user group name of this web content.
	*
	* @return the user group name of this web content
	*/
	@Override
	public java.lang.String getUserGroupName() {
		return _webContent.getUserGroupName();
	}

	/**
	* Sets the user group name of this web content.
	*
	* @param userGroupName the user group name of this web content
	*/
	@Override
	public void setUserGroupName(java.lang.String userGroupName) {
		_webContent.setUserGroupName(userGroupName);
	}

	/**
	* Returns the folder ID of this web content.
	*
	* @return the folder ID of this web content
	*/
	@Override
	public long getFolderId() {
		return _webContent.getFolderId();
	}

	/**
	* Sets the folder ID of this web content.
	*
	* @param folderId the folder ID of this web content
	*/
	@Override
	public void setFolderId(long folderId) {
		_webContent.setFolderId(folderId);
	}

	/**
	* Returns the folder name of this web content.
	*
	* @return the folder name of this web content
	*/
	@Override
	public java.lang.String getFolderName() {
		return _webContent.getFolderName();
	}

	/**
	* Sets the folder name of this web content.
	*
	* @param folderName the folder name of this web content
	*/
	@Override
	public void setFolderName(java.lang.String folderName) {
		_webContent.setFolderName(folderName);
	}

	@Override
	public boolean isNew() {
		return _webContent.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_webContent.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _webContent.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_webContent.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _webContent.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _webContent.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_webContent.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _webContent.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_webContent.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_webContent.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_webContent.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new WebContentWrapper((WebContent)_webContent.clone());
	}

	@Override
	public int compareTo(com.smartbics.model.WebContent webContent) {
		return _webContent.compareTo(webContent);
	}

	@Override
	public int hashCode() {
		return _webContent.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.smartbics.model.WebContent> toCacheModel() {
		return _webContent.toCacheModel();
	}

	@Override
	public com.smartbics.model.WebContent toEscapedModel() {
		return new WebContentWrapper(_webContent.toEscapedModel());
	}

	@Override
	public com.smartbics.model.WebContent toUnescapedModel() {
		return new WebContentWrapper(_webContent.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _webContent.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _webContent.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_webContent.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof WebContentWrapper)) {
			return false;
		}

		WebContentWrapper webContentWrapper = (WebContentWrapper)obj;

		if (Validator.equals(_webContent, webContentWrapper._webContent)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public WebContent getWrappedWebContent() {
		return _webContent;
	}

	@Override
	public WebContent getWrappedModel() {
		return _webContent;
	}

	@Override
	public void resetOriginalValues() {
		_webContent.resetOriginalValues();
	}

	private WebContent _webContent;
}
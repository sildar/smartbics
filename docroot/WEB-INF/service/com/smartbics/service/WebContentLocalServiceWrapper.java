/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartbics.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link WebContentLocalService}.
 *
 * @author ILDAR
 * @see WebContentLocalService
 * @generated
 */
public class WebContentLocalServiceWrapper implements WebContentLocalService,
	ServiceWrapper<WebContentLocalService> {
	public WebContentLocalServiceWrapper(
		WebContentLocalService webContentLocalService) {
		_webContentLocalService = webContentLocalService;
	}

	/**
	* Adds the web content to the database. Also notifies the appropriate model listeners.
	*
	* @param webContent the web content
	* @return the web content that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.smartbics.model.WebContent addWebContent(
		com.smartbics.model.WebContent webContent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.addWebContent(webContent);
	}

	/**
	* Creates a new web content with the primary key. Does not add the web content to the database.
	*
	* @param webContentId the primary key for the new web content
	* @return the new web content
	*/
	@Override
	public com.smartbics.model.WebContent createWebContent(long webContentId) {
		return _webContentLocalService.createWebContent(webContentId);
	}

	/**
	* Deletes the web content with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param webContentId the primary key of the web content
	* @return the web content that was removed
	* @throws PortalException if a web content with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.smartbics.model.WebContent deleteWebContent(long webContentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.deleteWebContent(webContentId);
	}

	/**
	* Deletes the web content from the database. Also notifies the appropriate model listeners.
	*
	* @param webContent the web content
	* @return the web content that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.smartbics.model.WebContent deleteWebContent(
		com.smartbics.model.WebContent webContent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.deleteWebContent(webContent);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _webContentLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.smartbics.model.WebContent fetchWebContent(long webContentId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.fetchWebContent(webContentId);
	}

	/**
	* Returns the web content with the primary key.
	*
	* @param webContentId the primary key of the web content
	* @return the web content
	* @throws PortalException if a web content with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.smartbics.model.WebContent getWebContent(long webContentId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.getWebContent(webContentId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the web contents.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of web contents
	* @param end the upper bound of the range of web contents (not inclusive)
	* @return the range of web contents
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.smartbics.model.WebContent> getWebContents(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.getWebContents(start, end);
	}

	/**
	* Returns the number of web contents.
	*
	* @return the number of web contents
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getWebContentsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.getWebContentsCount();
	}

	/**
	* Updates the web content in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param webContent the web content
	* @return the web content that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.smartbics.model.WebContent updateWebContent(
		com.smartbics.model.WebContent webContent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.updateWebContent(webContent);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _webContentLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_webContentLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _webContentLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.smartbics.model.WebContent update(long userGroupId,
		long contentFolderId,
		com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.update(userGroupId, contentFolderId,
			serviceContext);
	}

	@Override
	public long getContentFolder(long userGroupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.getContentFolder(userGroupId);
	}

	@Override
	public long getDefaultFolder()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.getDefaultFolder();
	}

	@Override
	public com.smartbics.model.WebContent updateDefaultFolder(
		long contentFolder,
		com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _webContentLocalService.updateDefaultFolder(contentFolder,
			serviceContext);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public WebContentLocalService getWrappedWebContentLocalService() {
		return _webContentLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedWebContentLocalService(
		WebContentLocalService webContentLocalService) {
		_webContentLocalService = webContentLocalService;
	}

	@Override
	public WebContentLocalService getWrappedService() {
		return _webContentLocalService;
	}

	@Override
	public void setWrappedService(WebContentLocalService webContentLocalService) {
		_webContentLocalService = webContentLocalService;
	}

	private WebContentLocalService _webContentLocalService;
}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartbics.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.smartbics.model.WebContent;

/**
 * The persistence interface for the web content service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author ILDAR
 * @see WebContentPersistenceImpl
 * @see WebContentUtil
 * @generated
 */
public interface WebContentPersistence extends BasePersistence<WebContent> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link WebContentUtil} to access the web content persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the web contents where userGroupId = &#63;.
	*
	* @param userGroupId the user group ID
	* @return the matching web contents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.smartbics.model.WebContent> findByUGID(
		long userGroupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the web contents where userGroupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userGroupId the user group ID
	* @param start the lower bound of the range of web contents
	* @param end the upper bound of the range of web contents (not inclusive)
	* @return the range of matching web contents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.smartbics.model.WebContent> findByUGID(
		long userGroupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the web contents where userGroupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userGroupId the user group ID
	* @param start the lower bound of the range of web contents
	* @param end the upper bound of the range of web contents (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching web contents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.smartbics.model.WebContent> findByUGID(
		long userGroupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first web content in the ordered set where userGroupId = &#63;.
	*
	* @param userGroupId the user group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching web content
	* @throws com.smartbics.NoSuchWebContentException if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent findByUGID_First(long userGroupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	/**
	* Returns the first web content in the ordered set where userGroupId = &#63;.
	*
	* @param userGroupId the user group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching web content, or <code>null</code> if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent fetchByUGID_First(long userGroupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last web content in the ordered set where userGroupId = &#63;.
	*
	* @param userGroupId the user group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching web content
	* @throws com.smartbics.NoSuchWebContentException if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent findByUGID_Last(long userGroupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	/**
	* Returns the last web content in the ordered set where userGroupId = &#63;.
	*
	* @param userGroupId the user group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching web content, or <code>null</code> if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent fetchByUGID_Last(long userGroupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the web contents before and after the current web content in the ordered set where userGroupId = &#63;.
	*
	* @param webContentId the primary key of the current web content
	* @param userGroupId the user group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next web content
	* @throws com.smartbics.NoSuchWebContentException if a web content with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent[] findByUGID_PrevAndNext(
		long webContentId, long userGroupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	/**
	* Removes all the web contents where userGroupId = &#63; from the database.
	*
	* @param userGroupId the user group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUGID(long userGroupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of web contents where userGroupId = &#63;.
	*
	* @param userGroupId the user group ID
	* @return the number of matching web contents
	* @throws SystemException if a system exception occurred
	*/
	public int countByUGID(long userGroupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the web content where userGroupId = &#63; or throws a {@link com.smartbics.NoSuchWebContentException} if it could not be found.
	*
	* @param userGroupId the user group ID
	* @return the matching web content
	* @throws com.smartbics.NoSuchWebContentException if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent findByuserGroupId(long userGroupId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	/**
	* Returns the web content where userGroupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param userGroupId the user group ID
	* @return the matching web content, or <code>null</code> if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent fetchByuserGroupId(long userGroupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the web content where userGroupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param userGroupId the user group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching web content, or <code>null</code> if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent fetchByuserGroupId(long userGroupId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the web content where userGroupId = &#63; from the database.
	*
	* @param userGroupId the user group ID
	* @return the web content that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent removeByuserGroupId(long userGroupId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	/**
	* Returns the number of web contents where userGroupId = &#63;.
	*
	* @param userGroupId the user group ID
	* @return the number of matching web contents
	* @throws SystemException if a system exception occurred
	*/
	public int countByuserGroupId(long userGroupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the web content where webContentId = &#63; or throws a {@link com.smartbics.NoSuchWebContentException} if it could not be found.
	*
	* @param webContentId the web content ID
	* @return the matching web content
	* @throws com.smartbics.NoSuchWebContentException if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent findByfolderId(long webContentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	/**
	* Returns the web content where webContentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param webContentId the web content ID
	* @return the matching web content, or <code>null</code> if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent fetchByfolderId(long webContentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the web content where webContentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param webContentId the web content ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching web content, or <code>null</code> if a matching web content could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent fetchByfolderId(long webContentId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the web content where webContentId = &#63; from the database.
	*
	* @param webContentId the web content ID
	* @return the web content that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent removeByfolderId(long webContentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	/**
	* Returns the number of web contents where webContentId = &#63;.
	*
	* @param webContentId the web content ID
	* @return the number of matching web contents
	* @throws SystemException if a system exception occurred
	*/
	public int countByfolderId(long webContentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the web content in the entity cache if it is enabled.
	*
	* @param webContent the web content
	*/
	public void cacheResult(com.smartbics.model.WebContent webContent);

	/**
	* Caches the web contents in the entity cache if it is enabled.
	*
	* @param webContents the web contents
	*/
	public void cacheResult(
		java.util.List<com.smartbics.model.WebContent> webContents);

	/**
	* Creates a new web content with the primary key. Does not add the web content to the database.
	*
	* @param webContentId the primary key for the new web content
	* @return the new web content
	*/
	public com.smartbics.model.WebContent create(long webContentId);

	/**
	* Removes the web content with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param webContentId the primary key of the web content
	* @return the web content that was removed
	* @throws com.smartbics.NoSuchWebContentException if a web content with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent remove(long webContentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	public com.smartbics.model.WebContent updateImpl(
		com.smartbics.model.WebContent webContent)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the web content with the primary key or throws a {@link com.smartbics.NoSuchWebContentException} if it could not be found.
	*
	* @param webContentId the primary key of the web content
	* @return the web content
	* @throws com.smartbics.NoSuchWebContentException if a web content with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent findByPrimaryKey(long webContentId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.smartbics.NoSuchWebContentException;

	/**
	* Returns the web content with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param webContentId the primary key of the web content
	* @return the web content, or <code>null</code> if a web content with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.smartbics.model.WebContent fetchByPrimaryKey(long webContentId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the web contents.
	*
	* @return the web contents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.smartbics.model.WebContent> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the web contents.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of web contents
	* @param end the upper bound of the range of web contents (not inclusive)
	* @return the range of web contents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.smartbics.model.WebContent> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the web contents.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of web contents
	* @param end the upper bound of the range of web contents (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of web contents
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.smartbics.model.WebContent> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the web contents from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of web contents.
	*
	* @return the number of web contents
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}
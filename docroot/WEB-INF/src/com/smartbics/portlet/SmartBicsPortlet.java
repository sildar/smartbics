package com.smartbics.portlet;

import java.io.IOException;

import com.smartbics.model.WebContent;
import com.smartbics.service.WebContentLocalServiceUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class SmartBicsPortlet
 */
public class SmartBicsPortlet extends MVCPortlet {

	public void render(
			RenderRequest renderRequest, RenderResponse renderResponse)
		throws PortletException, IOException {
		
		super.render(renderRequest, renderResponse);
	}
	
	
	public void updateFolder(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException, SystemException {
		ServiceContext serviceContext = ServiceContextFactory.getInstance(WebContent.class.getName(), actionRequest);
		
		long userGroup = ParamUtil.getLong(actionRequest, "userGroup");
		long contentFolder = ParamUtil.getLong(actionRequest, "contentFolder");
		long defaultFolder = ParamUtil.getLong(actionRequest, "defaultFolder");
		
		WebContentLocalServiceUtil.update(userGroup, contentFolder, serviceContext);
		if (defaultFolder != WebContentLocalServiceUtil.getDefaultFolder()){
			WebContentLocalServiceUtil.updateDefaultFolder(defaultFolder, serviceContext);
		}
		
		
	}

}

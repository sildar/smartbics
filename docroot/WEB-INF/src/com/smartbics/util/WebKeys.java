package com.smartbics.util;

public class WebKeys implements com.liferay.portal.kernel.util.WebKeys{

	public static final String DOCUMENT_LIBRARY_FOLDER = "DOCUMENT_LIBRARY_FOLDER";
	
	public static final String JOURNAL_FOLDER = "JOURNAL_FOLDER";
	
	public static final String ASSET_LAYOUT_TAG_NAMES = "ASSET_LAYOUT_TAG_NAMES";
}

/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartbics.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.smartbics.model.WebContent;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing WebContent in entity cache.
 *
 * @author ILDAR
 * @see WebContent
 * @generated
 */
public class WebContentCacheModel implements CacheModel<WebContent>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{webContentId=");
		sb.append(webContentId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", userGroupId=");
		sb.append(userGroupId);
		sb.append(", userGroupName=");
		sb.append(userGroupName);
		sb.append(", folderId=");
		sb.append(folderId);
		sb.append(", folderName=");
		sb.append(folderName);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public WebContent toEntityModel() {
		WebContentImpl webContentImpl = new WebContentImpl();

		webContentImpl.setWebContentId(webContentId);
		webContentImpl.setGroupId(groupId);
		webContentImpl.setCompanyId(companyId);
		webContentImpl.setUserId(userId);

		if (userName == null) {
			webContentImpl.setUserName(StringPool.BLANK);
		}
		else {
			webContentImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			webContentImpl.setCreateDate(null);
		}
		else {
			webContentImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			webContentImpl.setModifiedDate(null);
		}
		else {
			webContentImpl.setModifiedDate(new Date(modifiedDate));
		}

		webContentImpl.setUserGroupId(userGroupId);

		if (userGroupName == null) {
			webContentImpl.setUserGroupName(StringPool.BLANK);
		}
		else {
			webContentImpl.setUserGroupName(userGroupName);
		}

		webContentImpl.setFolderId(folderId);

		if (folderName == null) {
			webContentImpl.setFolderName(StringPool.BLANK);
		}
		else {
			webContentImpl.setFolderName(folderName);
		}

		webContentImpl.resetOriginalValues();

		return webContentImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		webContentId = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		userGroupId = objectInput.readLong();
		userGroupName = objectInput.readUTF();
		folderId = objectInput.readLong();
		folderName = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(webContentId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(userGroupId);

		if (userGroupName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userGroupName);
		}

		objectOutput.writeLong(folderId);

		if (folderName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(folderName);
		}
	}

	public long webContentId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long userGroupId;
	public String userGroupName;
	public long folderId;
	public String folderName;
}
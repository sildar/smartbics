/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartbics.service.impl;

import java.util.Date;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portlet.journal.service.JournalFolderServiceUtil;
import com.smartbics.NoSuchWebContentException;
import com.smartbics.model.WebContent;
import com.smartbics.service.base.WebContentLocalServiceBaseImpl;

/**
 * The implementation of the web content local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.smartbics.service.WebContentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author ILDAR
 * @see com.smartbics.service.base.WebContentLocalServiceBaseImpl
 * @see com.smartbics.service.WebContentLocalServiceUtil
 */
public class WebContentLocalServiceImpl extends WebContentLocalServiceBaseImpl {

	public WebContent update(long userGroupId, long contentFolderId, ServiceContext serviceContext) throws PortalException, SystemException{
		
		WebContent webContent = null;
		
		long increment = counterLocalService.increment(WebContent.class.getName());
		
		Date now = new Date();
		
		if (webContentPersistence.countByuserGroupId(userGroupId)>0) {
			webContent = webContentPersistence.findByuserGroupId(userGroupId);
			webContent.setModifiedDate(serviceContext.getCreateDate(now));
		}
		else{
			webContent = webContentPersistence.create(increment);
			webContent.setCreateDate(serviceContext.getCreateDate(now));
		}
		
		long companyId = serviceContext.getCompanyId();
		long groupId = serviceContext.getScopeGroupId();
		long userId = serviceContext.getGuestOrUserId();
		User user = userPersistence.findByPrimaryKey(userId);
		
		webContent.setCompanyId(companyId);
		webContent.setGroupId(groupId);
		webContent.setUserId(userId);
		webContent.setUserName(user.getFullName());
		
		UserGroup userGroup = null;
		try{
			userGroup = UserGroupLocalServiceUtil.getUserGroup(userGroupId-1);
		}catch (PortalException e) {
			e.printStackTrace();
		}
		
		String contentFolderName = JournalFolderServiceUtil.getFolder(contentFolderId).getName();
		
		webContent.setUserGroupId(userGroupId);
		webContent.setUserGroupName(userGroup.getName());
		webContent.setFolderId(contentFolderId);
		webContent.setFolderName(contentFolderName);
		
		webContentPersistence.update(webContent);
		
		return webContent;
		
	}
	
	public long getContentFolder(long userGroupId) throws SystemException{
		return webContentPersistence.findByUGID(userGroupId).get(0).getFolderId();
	}
	
	
	public long getDefaultFolder() throws SystemException{
		try {
			return webContentPersistence.findByfolderId(0).getFolderId();
		} catch (NoSuchWebContentException e) {
			WebContent webContent = webContentPersistence.create(0);
			webContent.setUserGroupId(0);
			webContent.setUserGroupName("Default content");
			webContentPersistence.update(webContent);
			return 0;
		}
	}
	
	public WebContent updateDefaultFolder(long contentFolder, ServiceContext serviceContext) throws SystemException{
		
		WebContent webContent = null;
		try {
			webContent = webContentPersistence.findByuserGroupId(0);
		} catch (NoSuchWebContentException e) {
			e.printStackTrace();
		}
		
		long companyId = serviceContext.getCompanyId();
		long groupId = serviceContext.getScopeGroupId();
		long userId = 0L;
		User user = null;
		
		try {
			userId = serviceContext.getGuestOrUserId();
			user = userPersistence.findByPrimaryKey(userId);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		 
		
		webContent.setCompanyId(companyId);
		webContent.setGroupId(groupId);
		webContent.setUserId(userId);
		webContent.setUserName(user.getFullName());
		Date now = new Date();
		webContent.setModifiedDate(serviceContext.getCreateDate(now));;
		
		
		webContent.setFolderId(contentFolder);
		webContentPersistence.update(webContent);
		
		return webContent;
	}
	
}
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartbics.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.smartbics.NoSuchWebContentException;

import com.smartbics.model.WebContent;
import com.smartbics.model.impl.WebContentImpl;
import com.smartbics.model.impl.WebContentModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the web content service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author ILDAR
 * @see WebContentPersistence
 * @see WebContentUtil
 * @generated
 */
public class WebContentPersistenceImpl extends BasePersistenceImpl<WebContent>
	implements WebContentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link WebContentUtil} to access the web content persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = WebContentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, WebContentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, WebContentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UGID = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, WebContentImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUGID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UGID = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, WebContentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUGID",
			new String[] { Long.class.getName() },
			WebContentModelImpl.USERGROUPID_COLUMN_BITMASK |
			WebContentModelImpl.FOLDERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UGID = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUGID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the web contents where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @return the matching web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WebContent> findByUGID(long userGroupId)
		throws SystemException {
		return findByUGID(userGroupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the web contents where userGroupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param start the lower bound of the range of web contents
	 * @param end the upper bound of the range of web contents (not inclusive)
	 * @return the range of matching web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WebContent> findByUGID(long userGroupId, int start, int end)
		throws SystemException {
		return findByUGID(userGroupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the web contents where userGroupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userGroupId the user group ID
	 * @param start the lower bound of the range of web contents
	 * @param end the upper bound of the range of web contents (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WebContent> findByUGID(long userGroupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UGID;
			finderArgs = new Object[] { userGroupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UGID;
			finderArgs = new Object[] { userGroupId, start, end, orderByComparator };
		}

		List<WebContent> list = (List<WebContent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (WebContent webContent : list) {
				if ((userGroupId != webContent.getUserGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_WEBCONTENT_WHERE);

			query.append(_FINDER_COLUMN_UGID_USERGROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(WebContentModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userGroupId);

				if (!pagination) {
					list = (List<WebContent>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WebContent>(list);
				}
				else {
					list = (List<WebContent>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first web content in the ordered set where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching web content
	 * @throws com.smartbics.NoSuchWebContentException if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent findByUGID_First(long userGroupId,
		OrderByComparator orderByComparator)
		throws NoSuchWebContentException, SystemException {
		WebContent webContent = fetchByUGID_First(userGroupId, orderByComparator);

		if (webContent != null) {
			return webContent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userGroupId=");
		msg.append(userGroupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWebContentException(msg.toString());
	}

	/**
	 * Returns the first web content in the ordered set where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching web content, or <code>null</code> if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent fetchByUGID_First(long userGroupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<WebContent> list = findByUGID(userGroupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last web content in the ordered set where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching web content
	 * @throws com.smartbics.NoSuchWebContentException if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent findByUGID_Last(long userGroupId,
		OrderByComparator orderByComparator)
		throws NoSuchWebContentException, SystemException {
		WebContent webContent = fetchByUGID_Last(userGroupId, orderByComparator);

		if (webContent != null) {
			return webContent;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userGroupId=");
		msg.append(userGroupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWebContentException(msg.toString());
	}

	/**
	 * Returns the last web content in the ordered set where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching web content, or <code>null</code> if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent fetchByUGID_Last(long userGroupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUGID(userGroupId);

		if (count == 0) {
			return null;
		}

		List<WebContent> list = findByUGID(userGroupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the web contents before and after the current web content in the ordered set where userGroupId = &#63;.
	 *
	 * @param webContentId the primary key of the current web content
	 * @param userGroupId the user group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next web content
	 * @throws com.smartbics.NoSuchWebContentException if a web content with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent[] findByUGID_PrevAndNext(long webContentId,
		long userGroupId, OrderByComparator orderByComparator)
		throws NoSuchWebContentException, SystemException {
		WebContent webContent = findByPrimaryKey(webContentId);

		Session session = null;

		try {
			session = openSession();

			WebContent[] array = new WebContentImpl[3];

			array[0] = getByUGID_PrevAndNext(session, webContent, userGroupId,
					orderByComparator, true);

			array[1] = webContent;

			array[2] = getByUGID_PrevAndNext(session, webContent, userGroupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected WebContent getByUGID_PrevAndNext(Session session,
		WebContent webContent, long userGroupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_WEBCONTENT_WHERE);

		query.append(_FINDER_COLUMN_UGID_USERGROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(WebContentModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userGroupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(webContent);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<WebContent> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the web contents where userGroupId = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUGID(long userGroupId) throws SystemException {
		for (WebContent webContent : findByUGID(userGroupId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(webContent);
		}
	}

	/**
	 * Returns the number of web contents where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @return the number of matching web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUGID(long userGroupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UGID;

		Object[] finderArgs = new Object[] { userGroupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_WEBCONTENT_WHERE);

			query.append(_FINDER_COLUMN_UGID_USERGROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userGroupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UGID_USERGROUPID_2 = "webContent.userGroupId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_USERGROUPID = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, WebContentImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByuserGroupId",
			new String[] { Long.class.getName() },
			WebContentModelImpl.USERGROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERGROUPID = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the web content where userGroupId = &#63; or throws a {@link com.smartbics.NoSuchWebContentException} if it could not be found.
	 *
	 * @param userGroupId the user group ID
	 * @return the matching web content
	 * @throws com.smartbics.NoSuchWebContentException if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent findByuserGroupId(long userGroupId)
		throws NoSuchWebContentException, SystemException {
		WebContent webContent = fetchByuserGroupId(userGroupId);

		if (webContent == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userGroupId=");
			msg.append(userGroupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchWebContentException(msg.toString());
		}

		return webContent;
	}

	/**
	 * Returns the web content where userGroupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userGroupId the user group ID
	 * @return the matching web content, or <code>null</code> if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent fetchByuserGroupId(long userGroupId)
		throws SystemException {
		return fetchByuserGroupId(userGroupId, true);
	}

	/**
	 * Returns the web content where userGroupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userGroupId the user group ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching web content, or <code>null</code> if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent fetchByuserGroupId(long userGroupId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { userGroupId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_USERGROUPID,
					finderArgs, this);
		}

		if (result instanceof WebContent) {
			WebContent webContent = (WebContent)result;

			if ((userGroupId != webContent.getUserGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_WEBCONTENT_WHERE);

			query.append(_FINDER_COLUMN_USERGROUPID_USERGROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userGroupId);

				List<WebContent> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERGROUPID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"WebContentPersistenceImpl.fetchByuserGroupId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					WebContent webContent = list.get(0);

					result = webContent;

					cacheResult(webContent);

					if ((webContent.getUserGroupId() != userGroupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERGROUPID,
							finderArgs, webContent);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERGROUPID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (WebContent)result;
		}
	}

	/**
	 * Removes the web content where userGroupId = &#63; from the database.
	 *
	 * @param userGroupId the user group ID
	 * @return the web content that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent removeByuserGroupId(long userGroupId)
		throws NoSuchWebContentException, SystemException {
		WebContent webContent = findByuserGroupId(userGroupId);

		return remove(webContent);
	}

	/**
	 * Returns the number of web contents where userGroupId = &#63;.
	 *
	 * @param userGroupId the user group ID
	 * @return the number of matching web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByuserGroupId(long userGroupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERGROUPID;

		Object[] finderArgs = new Object[] { userGroupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_WEBCONTENT_WHERE);

			query.append(_FINDER_COLUMN_USERGROUPID_USERGROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userGroupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERGROUPID_USERGROUPID_2 = "webContent.userGroupId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_FOLDERID = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, WebContentImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByfolderId",
			new String[] { Long.class.getName() },
			WebContentModelImpl.WEBCONTENTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FOLDERID = new FinderPath(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByfolderId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the web content where webContentId = &#63; or throws a {@link com.smartbics.NoSuchWebContentException} if it could not be found.
	 *
	 * @param webContentId the web content ID
	 * @return the matching web content
	 * @throws com.smartbics.NoSuchWebContentException if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent findByfolderId(long webContentId)
		throws NoSuchWebContentException, SystemException {
		WebContent webContent = fetchByfolderId(webContentId);

		if (webContent == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("webContentId=");
			msg.append(webContentId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchWebContentException(msg.toString());
		}

		return webContent;
	}

	/**
	 * Returns the web content where webContentId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param webContentId the web content ID
	 * @return the matching web content, or <code>null</code> if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent fetchByfolderId(long webContentId)
		throws SystemException {
		return fetchByfolderId(webContentId, true);
	}

	/**
	 * Returns the web content where webContentId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param webContentId the web content ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching web content, or <code>null</code> if a matching web content could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent fetchByfolderId(long webContentId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { webContentId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_FOLDERID,
					finderArgs, this);
		}

		if (result instanceof WebContent) {
			WebContent webContent = (WebContent)result;

			if ((webContentId != webContent.getWebContentId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_WEBCONTENT_WHERE);

			query.append(_FINDER_COLUMN_FOLDERID_WEBCONTENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(webContentId);

				List<WebContent> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FOLDERID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"WebContentPersistenceImpl.fetchByfolderId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					WebContent webContent = list.get(0);

					result = webContent;

					cacheResult(webContent);

					if ((webContent.getWebContentId() != webContentId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FOLDERID,
							finderArgs, webContent);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FOLDERID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (WebContent)result;
		}
	}

	/**
	 * Removes the web content where webContentId = &#63; from the database.
	 *
	 * @param webContentId the web content ID
	 * @return the web content that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent removeByfolderId(long webContentId)
		throws NoSuchWebContentException, SystemException {
		WebContent webContent = findByfolderId(webContentId);

		return remove(webContent);
	}

	/**
	 * Returns the number of web contents where webContentId = &#63;.
	 *
	 * @param webContentId the web content ID
	 * @return the number of matching web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByfolderId(long webContentId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FOLDERID;

		Object[] finderArgs = new Object[] { webContentId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_WEBCONTENT_WHERE);

			query.append(_FINDER_COLUMN_FOLDERID_WEBCONTENTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(webContentId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FOLDERID_WEBCONTENTID_2 = "webContent.webContentId = ?";

	public WebContentPersistenceImpl() {
		setModelClass(WebContent.class);
	}

	/**
	 * Caches the web content in the entity cache if it is enabled.
	 *
	 * @param webContent the web content
	 */
	@Override
	public void cacheResult(WebContent webContent) {
		EntityCacheUtil.putResult(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentImpl.class, webContent.getPrimaryKey(), webContent);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERGROUPID,
			new Object[] { webContent.getUserGroupId() }, webContent);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FOLDERID,
			new Object[] { webContent.getWebContentId() }, webContent);

		webContent.resetOriginalValues();
	}

	/**
	 * Caches the web contents in the entity cache if it is enabled.
	 *
	 * @param webContents the web contents
	 */
	@Override
	public void cacheResult(List<WebContent> webContents) {
		for (WebContent webContent : webContents) {
			if (EntityCacheUtil.getResult(
						WebContentModelImpl.ENTITY_CACHE_ENABLED,
						WebContentImpl.class, webContent.getPrimaryKey()) == null) {
				cacheResult(webContent);
			}
			else {
				webContent.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all web contents.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(WebContentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(WebContentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the web content.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(WebContent webContent) {
		EntityCacheUtil.removeResult(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentImpl.class, webContent.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(webContent);
	}

	@Override
	public void clearCache(List<WebContent> webContents) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (WebContent webContent : webContents) {
			EntityCacheUtil.removeResult(WebContentModelImpl.ENTITY_CACHE_ENABLED,
				WebContentImpl.class, webContent.getPrimaryKey());

			clearUniqueFindersCache(webContent);
		}
	}

	protected void cacheUniqueFindersCache(WebContent webContent) {
		if (webContent.isNew()) {
			Object[] args = new Object[] { webContent.getUserGroupId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERGROUPID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERGROUPID, args,
				webContent);

			args = new Object[] { webContent.getWebContentId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FOLDERID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FOLDERID, args,
				webContent);
		}
		else {
			WebContentModelImpl webContentModelImpl = (WebContentModelImpl)webContent;

			if ((webContentModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_USERGROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { webContent.getUserGroupId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERGROUPID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERGROUPID,
					args, webContent);
			}

			if ((webContentModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_FOLDERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { webContent.getWebContentId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FOLDERID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_FOLDERID, args,
					webContent);
			}
		}
	}

	protected void clearUniqueFindersCache(WebContent webContent) {
		WebContentModelImpl webContentModelImpl = (WebContentModelImpl)webContent;

		Object[] args = new Object[] { webContent.getUserGroupId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERGROUPID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERGROUPID, args);

		if ((webContentModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_USERGROUPID.getColumnBitmask()) != 0) {
			args = new Object[] { webContentModelImpl.getOriginalUserGroupId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERGROUPID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERGROUPID, args);
		}

		args = new Object[] { webContent.getWebContentId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FOLDERID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FOLDERID, args);

		if ((webContentModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_FOLDERID.getColumnBitmask()) != 0) {
			args = new Object[] { webContentModelImpl.getOriginalWebContentId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FOLDERID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_FOLDERID, args);
		}
	}

	/**
	 * Creates a new web content with the primary key. Does not add the web content to the database.
	 *
	 * @param webContentId the primary key for the new web content
	 * @return the new web content
	 */
	@Override
	public WebContent create(long webContentId) {
		WebContent webContent = new WebContentImpl();

		webContent.setNew(true);
		webContent.setPrimaryKey(webContentId);

		return webContent;
	}

	/**
	 * Removes the web content with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param webContentId the primary key of the web content
	 * @return the web content that was removed
	 * @throws com.smartbics.NoSuchWebContentException if a web content with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent remove(long webContentId)
		throws NoSuchWebContentException, SystemException {
		return remove((Serializable)webContentId);
	}

	/**
	 * Removes the web content with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the web content
	 * @return the web content that was removed
	 * @throws com.smartbics.NoSuchWebContentException if a web content with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent remove(Serializable primaryKey)
		throws NoSuchWebContentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			WebContent webContent = (WebContent)session.get(WebContentImpl.class,
					primaryKey);

			if (webContent == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchWebContentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(webContent);
		}
		catch (NoSuchWebContentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected WebContent removeImpl(WebContent webContent)
		throws SystemException {
		webContent = toUnwrappedModel(webContent);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(webContent)) {
				webContent = (WebContent)session.get(WebContentImpl.class,
						webContent.getPrimaryKeyObj());
			}

			if (webContent != null) {
				session.delete(webContent);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (webContent != null) {
			clearCache(webContent);
		}

		return webContent;
	}

	@Override
	public WebContent updateImpl(com.smartbics.model.WebContent webContent)
		throws SystemException {
		webContent = toUnwrappedModel(webContent);

		boolean isNew = webContent.isNew();

		WebContentModelImpl webContentModelImpl = (WebContentModelImpl)webContent;

		Session session = null;

		try {
			session = openSession();

			if (webContent.isNew()) {
				session.save(webContent);

				webContent.setNew(false);
			}
			else {
				session.merge(webContent);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !WebContentModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((webContentModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UGID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						webContentModelImpl.getOriginalUserGroupId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UGID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UGID,
					args);

				args = new Object[] { webContentModelImpl.getUserGroupId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UGID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UGID,
					args);
			}
		}

		EntityCacheUtil.putResult(WebContentModelImpl.ENTITY_CACHE_ENABLED,
			WebContentImpl.class, webContent.getPrimaryKey(), webContent);

		clearUniqueFindersCache(webContent);
		cacheUniqueFindersCache(webContent);

		return webContent;
	}

	protected WebContent toUnwrappedModel(WebContent webContent) {
		if (webContent instanceof WebContentImpl) {
			return webContent;
		}

		WebContentImpl webContentImpl = new WebContentImpl();

		webContentImpl.setNew(webContent.isNew());
		webContentImpl.setPrimaryKey(webContent.getPrimaryKey());

		webContentImpl.setWebContentId(webContent.getWebContentId());
		webContentImpl.setGroupId(webContent.getGroupId());
		webContentImpl.setCompanyId(webContent.getCompanyId());
		webContentImpl.setUserId(webContent.getUserId());
		webContentImpl.setUserName(webContent.getUserName());
		webContentImpl.setCreateDate(webContent.getCreateDate());
		webContentImpl.setModifiedDate(webContent.getModifiedDate());
		webContentImpl.setUserGroupId(webContent.getUserGroupId());
		webContentImpl.setUserGroupName(webContent.getUserGroupName());
		webContentImpl.setFolderId(webContent.getFolderId());
		webContentImpl.setFolderName(webContent.getFolderName());

		return webContentImpl;
	}

	/**
	 * Returns the web content with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the web content
	 * @return the web content
	 * @throws com.smartbics.NoSuchWebContentException if a web content with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent findByPrimaryKey(Serializable primaryKey)
		throws NoSuchWebContentException, SystemException {
		WebContent webContent = fetchByPrimaryKey(primaryKey);

		if (webContent == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchWebContentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return webContent;
	}

	/**
	 * Returns the web content with the primary key or throws a {@link com.smartbics.NoSuchWebContentException} if it could not be found.
	 *
	 * @param webContentId the primary key of the web content
	 * @return the web content
	 * @throws com.smartbics.NoSuchWebContentException if a web content with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent findByPrimaryKey(long webContentId)
		throws NoSuchWebContentException, SystemException {
		return findByPrimaryKey((Serializable)webContentId);
	}

	/**
	 * Returns the web content with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the web content
	 * @return the web content, or <code>null</code> if a web content with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		WebContent webContent = (WebContent)EntityCacheUtil.getResult(WebContentModelImpl.ENTITY_CACHE_ENABLED,
				WebContentImpl.class, primaryKey);

		if (webContent == _nullWebContent) {
			return null;
		}

		if (webContent == null) {
			Session session = null;

			try {
				session = openSession();

				webContent = (WebContent)session.get(WebContentImpl.class,
						primaryKey);

				if (webContent != null) {
					cacheResult(webContent);
				}
				else {
					EntityCacheUtil.putResult(WebContentModelImpl.ENTITY_CACHE_ENABLED,
						WebContentImpl.class, primaryKey, _nullWebContent);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(WebContentModelImpl.ENTITY_CACHE_ENABLED,
					WebContentImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return webContent;
	}

	/**
	 * Returns the web content with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param webContentId the primary key of the web content
	 * @return the web content, or <code>null</code> if a web content with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WebContent fetchByPrimaryKey(long webContentId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)webContentId);
	}

	/**
	 * Returns all the web contents.
	 *
	 * @return the web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WebContent> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the web contents.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of web contents
	 * @param end the upper bound of the range of web contents (not inclusive)
	 * @return the range of web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WebContent> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the web contents.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.smartbics.model.impl.WebContentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of web contents
	 * @param end the upper bound of the range of web contents (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WebContent> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<WebContent> list = (List<WebContent>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_WEBCONTENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_WEBCONTENT;

				if (pagination) {
					sql = sql.concat(WebContentModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<WebContent>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WebContent>(list);
				}
				else {
					list = (List<WebContent>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the web contents from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (WebContent webContent : findAll()) {
			remove(webContent);
		}
	}

	/**
	 * Returns the number of web contents.
	 *
	 * @return the number of web contents
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_WEBCONTENT);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the web content persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.smartbics.model.WebContent")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<WebContent>> listenersList = new ArrayList<ModelListener<WebContent>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<WebContent>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(WebContentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_WEBCONTENT = "SELECT webContent FROM WebContent webContent";
	private static final String _SQL_SELECT_WEBCONTENT_WHERE = "SELECT webContent FROM WebContent webContent WHERE ";
	private static final String _SQL_COUNT_WEBCONTENT = "SELECT COUNT(webContent) FROM WebContent webContent";
	private static final String _SQL_COUNT_WEBCONTENT_WHERE = "SELECT COUNT(webContent) FROM WebContent webContent WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "webContent.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No WebContent exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No WebContent exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(WebContentPersistenceImpl.class);
	private static WebContent _nullWebContent = new WebContentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<WebContent> toCacheModel() {
				return _nullWebContentCacheModel;
			}
		};

	private static CacheModel<WebContent> _nullWebContentCacheModel = new CacheModel<WebContent>() {
			@Override
			public WebContent toEntityModel() {
				return _nullWebContent;
			}
		};
}
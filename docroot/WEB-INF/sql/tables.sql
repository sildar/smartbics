create table smart_WebContent (
	webContentId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	userGroupId LONG,
	userGroupName VARCHAR(75) null,
	folderId LONG,
	folderName VARCHAR(75) null
);
<%@ include file="../init.jsp" %>


<%
	PortletURL portletURL = renderResponse.createRenderURL();
	
	SearchContainer searchContainer = new SearchContainer(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, pageDelta, portletURL, null, null);
	
	if (!paginationType.equals("none")) {
		searchContainer.setDelta(pageDelta);
		searchContainer.setDeltaConfigurable(false);
	}
%>


<%
	List results = null;
	int total = 0;
	int status = WorkflowConstants.STATUS_APPROVED;
	total = JournalFolderServiceUtil.getFoldersAndArticlesCount(scopeGroupId, themeDisplay.getUserId(), selectFolderID, status);
	searchContainer.setTotal(total);
	results = JournalFolderServiceUtil.getFoldersAndArticles(scopeGroupId, themeDisplay.getUserId(), selectFolderID, status, searchContainer.getStart(), searchContainer.getEnd(), searchContainer.getOrderByComparator());
%>
	
	
<%
	searchContainer.setResults(results);
%>
	
<c:if test="<%= results.isEmpty() %>">
	<div class="entries-empty alert alert-info">
		<liferay-ui:message key="no-web-content-was-found" />
	</div>
</c:if>
	

<%
for (int i = 0; i < results.size(); i++) {
	Object result = results.get(i);
	
	JournalArticle curArticle = null;
	JournalFolder curFolder = null;

	if (result instanceof JournalFolder) {
		curFolder = (JournalFolder)result;
		continue;
	}
	else {
		curArticle = (JournalArticle)result;
	}
%>



	<liferay-util:buffer var="articleTitle">

		<%
			PortletURL rowURL = liferayPortletResponse.createRenderURL();

			rowURL.setParameter("mvcPath", "/jsp/smart/article.jsp");
			rowURL.setParameter("groupId", String.valueOf(curArticle.getGroupId()));
			rowURL.setParameter("folderId", String.valueOf(curArticle.getFolderId()));
			rowURL.setParameter("articleId", curArticle.getArticleId());

			rowURL.setParameter("status", String.valueOf(curArticle.getStatus()));
		%>

		<liferay-ui:icon
			image="../file_system/small/html"
			label="<%= true %>"
			message="<%= HtmlUtil.escape(curArticle.getTitle(locale)) %>"
			method="get"
			url="<%= rowURL.toString() %>"
		/>

		<c:if test="<%= curArticle.getGroupId() != scopeGroupId %>">
			<small class="group-info">
				<dl>

					<%
					Group group = GroupLocalServiceUtil.getGroup(curArticle.getGroupId());
					%>

					<c:if test="<%= !group.isLayout() || (group.getParentGroupId() != scopeGroupId) %>">
						<dt>
							<liferay-ui:message key="site" />:
						</dt>

						<dd>

							<%
							String groupDescriptiveName = null;

							if (group.isLayout()) {
								Group parentGroup = group.getParentGroup();

								groupDescriptiveName = parentGroup.getDescriptiveName(locale);
							}
							else {
								groupDescriptiveName = group.getDescriptiveName(locale);
							}
							%>

							<%= HtmlUtil.escape(groupDescriptiveName) %>
						</dd>
					</c:if>

					<c:if test="<%= group.isLayout() %>">
						<dt>
							<liferay-ui:message key="scope" />:
						</dt>

						<dd>
							<%= HtmlUtil.escape(group.getDescriptiveName(locale)) %>
						</dd>
					</c:if>
				</dl>
			</small>
		</c:if>
	</liferay-util:buffer>



	<%
		List resultRows = searchContainer.getResultRows();

		ResultRow row = new ResultRow(curArticle, curArticle.getArticleId(), i);

		row.setClassName("entry-display-style selectable");

		Map<String, Object> data = new HashMap<String, Object>();

		data.put("title", HtmlUtil.escape(curArticle.getTitle(locale)));

		row.setData(data);
		
		
		// Article id

		row.addText(HtmlUtil.escape(curArticle.getArticleId()));

		// Title

		TextSearchEntry articleTitleTextSearchEntry = new TextSearchEntry();

		articleTitleTextSearchEntry.setName(articleTitle);

		row.addSearchEntry(articleTitleTextSearchEntry);

		// Modified date

		row.addDate(curArticle.getModifiedDate());

		// Display date

		row.addDate(curArticle.getDisplayDate());

		// Author

		row.addText(PortalUtil.getUserName(curArticle));

		// Add result row

		resultRows.add(row);
	%>

<%
	}
%>

<liferay-ui:search-iterator paginate="<%= false %>" searchContainer="<%= searchContainer %>" />

<%@ include file="../init.jsp" %>


<liferay-portlet:actionURL name="updateFolder" var="updateFolderURL" />


<aui:form action="<%= updateFolderURL %>" method="post" name="fm" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "saveConfiguration();" %>'>

	<%
		JournalFolder folder = (JournalFolder)request.getAttribute(WebKeys.JOURNAL_FOLDER);
		
		long folderId = BeanParamUtil.getLong(folder, request, "folderId", JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID);
		
		String eventName = ParamUtil.getString(request, "eventName", "_15_selectFolder");
		
		String folderName = LanguageUtil.get(pageContext, "home");
		
		if (folder != null) {
			folderName = folder.getName();
		}

		int total = JournalFolderServiceUtil.getFoldersCount(scopeGroupId, folderId);
		List<JournalFolder> results = JournalFolderServiceUtil.getFolders(scopeGroupId, folderId);
		
	%>
	<aui:fieldset>
		<aui:select label="default Folder" name="defaultFolder">
			<%
				for(Iterator<JournalFolder> i = results.iterator(); i.hasNext(); ) {
					JournalFolder item = i.next();
			%>
				<aui:option label="<%= item.getName() %>" selected="<%= defaultFolderID==item.getFolderId() %>" value="<%= item.getFolderId() %>" />
			<%
				}
			%>
		</aui:select>
	</aui:fieldset>
	
	<hr size="3px" color="#F00">
	
	<%
		//userGroups
		List<UserGroup> userGroups = UserGroupLocalServiceUtil.getUserGroups(themeDisplay.getCompanyId());
	%>
	
	<aui:fieldset>
		<aui:select label="userGroups" name="userGroup" id="userGroupsOptions">
			<%
			for(Iterator<UserGroup> i = userGroups.iterator(); i.hasNext(); ) {
				UserGroup userGroup = i.next();
			%>
				<aui:option label="<%= userGroup.getName() %>" selected="<%= false %>" value="<%= userGroup.getGroupId() %>" />
			<%
			}
			%>
		</aui:select>
	</aui:fieldset>
	
	<aui:fieldset>
		<aui:select label="select Folder" name="contentFolder" id="contentFolderOptions" >
			<%= _buildSubFolders(scopeGroupId, folderId) %>
		</aui:select>
	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>


<aui:script>
	var webContent = {};
</aui:script>
<%
	for (Iterator<WebContent> oo = webContents.iterator(); oo.hasNext();){
		WebContent item = oo.next();
		
		%>
		<aui:script>
			webContent[<%= item.getUserGroupId() %>] = '<%= item.getFolderId() %>';
		</aui:script>
		<%
	}
%>
<aui:script>
	Liferay.provide(
		window,
		'<portlet:namespace />saveConfiguration',
		function() {
			submitForm(document.<portlet:namespace />fm);
		}
	);
</aui:script>
<aui:script use="aui-base, node, event">
	var userGroupsOptions = A.one('#<portlet:namespace/>userGroupsOptions');
	var contentFolderOptions = A.one('#<portlet:namespace/>contentFolderOptions');
	userGroupsOptions.on('change', function(event){
		var tmp = webContent[userGroupsOptions.get("value")];
		contentFolderOptions.set("value",tmp);
    });
</aui:script>


<%!
private String _buildSubFolders(long scopeGroupId, long folderId) throws Exception {
	StringBundler sb = new StringBundler();
	
	 for (JournalFolder tmp : JournalFolderServiceUtil.getFolders(scopeGroupId, folderId))
	 {	
		 List<JournalFolder> listSubFolders = JournalFolderServiceUtil.getFolders(scopeGroupId, tmp.getFolderId());
		 sb.append("<option label='"+tmp.getName()+"' value='"+tmp.getFolderId()+"' />");
		 	
		 	if(!listSubFolders.isEmpty()){
		 		sb.append(_buildSubFolders(scopeGroupId, tmp.getFolderId()));
		 	}
	 }
	 
	 return sb.toString();
	}
%>
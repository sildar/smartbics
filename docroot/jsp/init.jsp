<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />


<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Date"%>

<%@page import="javax.portlet.PortletURL"%>

<%@page import="com.liferay.portal.kernel.bean.BeanParamUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletResponse"%>
<%@page import="com.liferay.portal.kernel.repository.model.Folder"%>
<%@page import="com.liferay.portal.kernel.search.Hits"%>

<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.PrefsParamUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portal.kernel.dao.search.TextSearchEntry"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>

<%@page import="com.liferay.portal.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.security.permission.ActionKeys"%>
<%@page import="com.liferay.portal.service.GroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Group"%>

<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.util.PortletKeys"%>


<%@page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@page import="com.liferay.portlet.asset.util.AssetUtil"%>
<%@page import="com.liferay.portlet.asset.service.persistence.AssetEntryQuery"%>

<%@page import="com.liferay.portlet.journal.service.JournalArticleServiceUtil"%>
<%@page import="com.liferay.portlet.journal.service.JournalFolderServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalFolder"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>

<%@page import="com.liferay.portlet.documentlibrary.NoSuchFolderException"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFolderConstants"%>


<%@page import="com.liferay.portal.kernel.util.KeyValuePair"%>
<%@page import="com.liferay.portal.model.UserGroup"%>
<%@page import="com.liferay.portal.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.util.StringBundler"%>

<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>

<%@page import="com.liferay.portlet.journal.NoSuchArticleException"%>
<%@page import="com.liferay.portal.layoutconfiguration.util.RuntimePageUtil"%>

<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticleDisplay"%>
<%@page import="com.liferay.util.portlet.PortletRequestUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>

<%@page import="com.smartbics.util.WebKeys"%>
<%@page import="com.smartbics.util.JournalFolderConstants"%>
<%@page import="com.smartbics.service.WebContentLocalServiceUtil"%>
<%@page import="com.smartbics.model.WebContent"%>

<%
	List<WebContent> webContents = WebContentLocalServiceUtil.getWebContents(-1, -1);

	String portletResource = ParamUtil.getString(request, "portletResource");

	long defaultFolderID = WebContentLocalServiceUtil.getDefaultFolder();
	long selectFolderID = defaultFolderID;
	
	if (themeDisplay.isSignedIn() && user.getUserGroups().size()>0){
		selectFolderID = WebContentLocalServiceUtil.getContentFolder(user.getUserGroups().get(0).getUserGroupId()+1);
	}
	
	long rootFolderId = PrefsParamUtil.getLong(portletPreferences, request, "rootFolderId", DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
	String rootFolderName = StringPool.BLANK;

	if (rootFolderId != DLFolderConstants.DEFAULT_PARENT_FOLDER_ID) {
		try {
			Folder rootFolder = DLAppLocalServiceUtil.getFolder(rootFolderId);
	
			rootFolderName = rootFolder.getName();
		}
		catch (NoSuchFolderException nsfe) {
		}
	}
	
	int pageDelta = GetterUtil.getInteger(portletPreferences.getValue("pageDelta", portletPreferences.getValue("delta", null)), 20);
	String paginationType = GetterUtil.getString(portletPreferences.getValue("paginationType", "none"));
%>